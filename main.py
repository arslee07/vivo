from telethon import TelegramClient, events, types
from os import environ

client = TelegramClient("vivo", environ["API_ID"], environ["API_HASH"])


@client.on(events.NewMessage(outgoing=True))
async def handler(event: types.Message):
    try:
        await event.edit(event.message.message + "\nСмартфон vivo")
    except Exception:
        pass  # и че


client.start()
client.run_until_disconnected()
